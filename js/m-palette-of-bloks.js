﻿var module = angular.module("mPaletteOfBloks", ['lvl.services']);

module.directive('paletteOfBloks', function(){
		return {
          restrict: 'E',
          templateUrl: 'js/paletteOfBloksTemplate.html',
          scope: {
          	data:'=',
          },

          controllerAs: 'PaletteOfBloksController',
          controller: function($scope){
          		//console.log('palette controller data ='); console.log($scope.data)
          }
      }
	})