﻿var module = angular.module("mSlotBlock", ['lvl.services']);

module.directive('slotBlock', ['$rootScope', 'uuid', function($rootScope, uuid) {
	    return {
	        restrict: 'E',
			controllerAs: 'SlotBlockController',
			templateUrl: 'js/slotBlockTemplate.html',
		    scope: {      			
			  data : '=',	
			  isicon : '=',		  
			},
	        controller: function($scope) {
				console.log('in slot controller');						
					        	
	        }
    	}
	}]);
	