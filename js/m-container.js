var module = angular.module("mContainer", ['lvl.services']);

module.directive('container', ['$rootScope', 'uuid', function($rootScope, uuid) {
	    return {
	        restrict: 'E',
			controllerAs: 'ContainerController',
			templateUrl: 'js/containerTemplate.html',
			scope: {
				data: "=",					
			},

	        controller: function($scope,servDesk) { //,servContainer
				console.log('container controller data ='); console.log($scope.data)
				var t = this;
								
				/* 
				 * Добавляем временные блоки-гнезда между текущими
				 */
				$scope.addTempSlots2 = function(){
					$scope.$apply(function() {
						servDesk.addTempSlots($scope.data);
					})
					console.log('addTempSlots - new data'); console.log($scope.data);
				}

				/*удаляем все временные блоки*/
				$scope.removeTempSlots = function(){
					$scope.$apply(function() {
						servDesk.removeTempSlots($scope.data);
					});
					console.log('removeTempSlots - new data'); console.log($scope.data);
				}
					
				/*потащили блок*/
				$rootScope.$on('uni-block.dragstart',function(ev,inf) {					
					$scope.addTempSlots2();
				})

				/*отпустили перетаскиваемый блок*/
				$rootScope.$on('uni-block.dragend',function(ev,inf) {					
					$scope.removeTempSlots();
				})

				/*запускаем удаление по dragenter на один из блоков*/
				//$scope.$on('uni-block.dragenter',function(){
				$rootScope.$on('uni-block.drop',function(ev,inf){
					//alert('catching uni-block.drop');
					console.log('event:catching uni-block.drop');console.log(inf);
					
					//ищем индексы таскаемого и принимающего в палитре и контейнере
					var inPaletId = objIndexOf($scope.data.palette, inf.src_id);
					/*if (inPaletId === -1) {
						var inPaletId = objIndexOf($scope.data.container, inf.src_id);
					}*/
					var inContId = objIndexOf($scope.data.container, inf.dest_id);

					console.log('inPaletId='+inPaletId+'; inContId='+inContId);
					$scope.$apply(function() {
						angular.copy($scope.data.palette[inPaletId],$scope.data.container[inContId]);
						$scope.data.container[inContId].id = uuid.new();

						$scope.data.container[inContId].isicon = 0;
						$scope.data.container[inContId].status = 'permanent';					
					})
					$scope.removeTempSlots();
				});

	        	/*Привязыватель обрабочиков на DROP. Вызывать в link*/
	        	$scope.bindDropHandlers = function(scope, el, attrs, controller) {
		            var id = angular.element(el).attr("id");
		            /*if (!id) {
		                id = uuid.new()
		                angular.element(el).attr("id", id);
		            }*/
		                       
		            el.bind("dragover", function(e) {
		              if (e.preventDefault) {
		                e.preventDefault(); // Necessary. Allows us to drop.
		              }
		              
		              e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
		              return false;
		            });
		            
		            el.bind("dragenter", function(e) {
		              // this / e.target is the current hover target.
		              angular.element(e.target).addClass('lvl-over');
		              //servDeskInfra.setInContainerFlag(true);
		              //$scope.addTempSlots();

		            });
		            
		            el.bind("dragleave", function(e) {
		              angular.element(e.target).removeClass('lvl-over');  // this / e.target is previous target element.
		              //servDeskInfra.setInContainerFlag(false);
		              //$scope.removeTempSlots();
		            });
		            
		            el.bind("drop", function(e) {
		              if (e.preventDefault) {
		                e.preventDefault(); // Necessary. Allows us to drop.
		              }

		              if (e.stopPropogation) {
		                e.stopPropogation(); // Necessary. Allows us to drop.
		              }
		            	var data = e.dataTransfer.getData("text");
		                // var dest = document.getElementById(id);
		                // var src = document.getElementById(data);
		                var dest_id = id;
		                var src_id = data;
		                
		                //$scope.onDrop({dragEl: src_id, dropEl: dest_id});
		            });
		            
		            $rootScope.$on("LVL-DRAG-END", function() {
		                var el = document.getElementById(id);
		                angular.element(el).removeClass("lvl-target");
		                angular.element(el).removeClass("lvl-over");
		            });
		        }

		        /**
		         * удаляет из контейнера блок по id
				 */
		        $scope.removeBlock = function (id) {	        	
		        	console.log('removing block id='+id);
		        	$scope.$apply(function() {
						servDesk.removeBlock($scope.data,id);
					});
		        }

		        /* По событию-запросу удалению блока - удаляем из модели */
				$rootScope.$on('askBlockRemove',function(ev,dat){  										
					console.log('trying removing block');
					//$scope.removeBlock(dat['id']);    
					servDesk.removeBlock($scope.data, dat['id']);     				  
				})

	        },	        

	        link: function(scope, el, attrs, controller){	        	
	        	//console.log(scope);	        	
	        	scope.bindDropHandlers(scope, el, attrs, controller);
	        }    		 
  
		}
	}])
