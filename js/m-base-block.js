﻿var module = angular.module("mBaseBlock", ['lvl.services']);

module.directive('baseBlock', ['$rootScope', 'uuid', function($rootScope, uuid) {
	    return {
	        restrict: 'E',
			controllerAs: 'BaseBlockController',
			templateUrl: 'js/baseBlockTemplate.html',
		    scope: {      
			  id : '=',
			  data : '=',
			  toggleTab : '&',
			  render : '&',	
			  isicon :	'=',			  
			  status :	'=',//постоянный-временный -- пока не используется	  
			},			
	        controller: function($scope,servBaseBlock) {								
				var t = this;

				console.log('baseBlock controller isicon = '); console.log($scope.isicon); 
				//t.id = uuid.new();
				t.id = $scope.id;	
				
				console.log('baseBlock controller data = '); console.log($scope.data);
				t.data = $scope.data;
				
				//данные модели в виде вкладок
				//t.data = servBaseBlock.getBlock();
				//обновим вкладку для рендера браузером				
				servBaseBlock.updateRenderData(t.data);				
				
				/*показ содержимого активной вкладки*/
				t.toggleTab = function(n){					
					for (i in t.data){
						if (i === n) {
							t.data[i]['active'] = 1;
						} else {
							t.data[i]['active'] = 0;
						}
					}					
				}
				
				/*покажем во вкладке render что получилось*/
				t.render = function(){					
					//обновим вкладку для рендера браузером				
					servBaseBlock.updateRenderData(t.data);				
				}

				t.askRemoving = function(){
					//console.log('12, id='+t.id); 
					//кидаем событие - удаление блока
					$rootScope.$broadcast('askBlockRemove',{'id':t.id});					
				}

				// t.ondrop = function (drag_id, drop_id) {
				// 	console.log('dropped!');
				// }
	        	
	        }
    	}
	}]);
	
//служба, добывающая данные  и отчасти обрабатывающая их
module.service('servBaseBlock',function($http, $sce){
  /*this.getBlock = function(id){
    
	//if id = undefined ...
	
	var data = {
		render : {id:0, title:'Render', content:'', active:1},
		html   : {id:1, title:'HTML', content:'это <i>html</i>', active:0},
		css    : {id:2, title:'CSS', content:'i{color:red;}', active:0},
		js     : {id:3, title:'JS', content:'console.log($("i").text());', active:0}
	}
	
    return data;
  };*/
  
  /*склейка всего в блок для рендера браузером*/
  this.updateRenderData = function(data){		
	if (typeof(data) !== 'undefined'){
		data.render.content = $sce.trustAsHtml(
			data.html.content 
			+ '<style>' + data.css.content + '</style>'
			+ '<script>' + data.js.content + '</script>'
		);	
	}
	return data;	
  }
  
  
})
