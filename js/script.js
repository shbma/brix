/**
 * поиск по id в массиве объектов
 *
 * @param arr  массив объектов
 * @param id  айдишник, который ищем
 * @return индекс объекта с данным id или -1, если не найдено
 */
function objIndexOf(arr, id) {
for(var idx = 0, l = arr.length;arr[idx] && arr[idx].id !== id;idx++);
	return idx === l ? -1 : idx;
};	

angular
	.module('ddApp', [ // register
		'BuDirectives',
		'mContainer','mPaletteOfBloks','mDesk',
		'mBaseBlock','mUniBlock','mSlotBlock'
	]) 
	.run(function($rootScope, $templateCache) {		
		//Данный подход удаляет все созданные шаблоны из кэша при каждом обновлении ngView
		$rootScope.$on('$viewContentLoaded', function() {
		  $templateCache.removeAll();
		});

		/*
		//Но, это не очень удобный метод решения задачи, поскольку нам нужно 
		//чтобы некоторые шаблоны все таки оставались и не собирались заново. 
		//Решение – настроить так, чтобы записи кэша удалялись по изменению 
		//маршрута и только те которые нужно удалить:
		//http://ntischuk.com/2015/04/03/javascript-angularjs-caching-templates/
		
	    $rootScope.$on('$routeChangeStart', function(event, next, current) {
	        if (typeof(current) == 'object' && 'unsetCache' in current && current.unsetCache == true){
	            $templateCache.remove(current.templateUrl);
	        }
	    });
		*/
	});

	