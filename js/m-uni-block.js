var module = angular.module("mUniBlock", ['lvl.services']);

module.directive('uniBlock', ['$rootScope', 'uuid', function($rootScope, uuid) {
	    return {
	        restrict: 'A',
			controllerAs: 'UniBlockController',			
			scope:{
				block:'=',							
			},

	        controller: function($scope,$rootScope) {
	        	var t = this;	
	        	console.log('uniBlockController block = '); console.log($scope.block);		

	        	/*обработчик Drop-а на элементе*/
	        	$scope.onDrop = function(persons){
	        		console.log('Dropped!');
	        		var src_id = persons.dragEl_id;
	        		var dest_id = persons.dropEl_id;
	        		console.log('src_id ' + src_id + 'dest_id ' + dest_id);
	        		console.log('this.id ' + $scope.id);	        		

	        		$rootScope.$broadcast('uni-block.drop',{
	        			'src_id':src_id,
	        			'dest_id':dest_id,
	        		});
	        	}

				/*Привязыватель обрабочиков на DRAG. Вызывать в link*/
				$scope.bindDragHandlers = function(scope, el, attrs, controller) {					
		        	console.log("linking draggable element");

		            angular.element(el).attr("draggable", "true");
		            //var id = angular.element(el).attr("id");
		            
		            el.bind("dragstart", function(e) {
		            	console.log('drag starts!'); 
		                e.dataTransfer.setData('text', e.target.id);
		                console.log('id='+e.target.id);	

		                $rootScope.$broadcast('uni-block.dragstart',{});                
		            });
		            
		            el.bind("dragend", function(e) {
		                $rootScope.$broadcast('uni-block.dragend',{});
		            });
		        }	

				/*Привязыватель обрабочиков на DROP. Вызывать в link*/
		        $scope.bindDropHandlers = function(scope, el, attrs, controller) {
		            		                       
		            el.bind("dragover", function(e) {
		              if (e.preventDefault) {
		                e.preventDefault(); // Necessary. Allows us to drop.
		              }
		              
		              e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
		              return false;
		            });
		            
		            el.bind("dragenter", function(e) {
		              // this / e.target is the current hover target.
		              //$scope.$emit('uni-block.dragenter','block is choosen');		             
		              angular.element(e.target).addClass('lvl-over');

		            });
		            
		            el.bind("dragleave", function(e) {
		              angular.element(e.target).removeClass('lvl-over');  // this / e.target is previous target element.
		            });
		            
		            el.bind("drop", function(e) {		              
		              if (e.preventDefault) {
		                e.preventDefault(); // Necessary. Allows us to drop.
		              }

		              if (e.stopPropogation) {
		                e.stopPropogation(); // Necessary. Allows us to drop.
		              }
		              console.log("drop in uni-block");console.log(e);
		            	var data = e.dataTransfer.getData("text");
	
		                var dest_id = angular.element(el).attr('id');
		                var src_id = data;
		                
		                $scope.onDrop({dragEl_id: src_id, dropEl_id: dest_id});		                
		            });

					id = angular.element(el).attr("id");
		            $rootScope.$on("LVL-DRAG-START", function() {
		                var el = document.getElementById(id);
		                angular.element(el).addClass("lvl-target");
		            });
		            
		            $rootScope.$on("LVL-DRAG-END", function() {
		                var el = document.getElementById(id);
		                angular.element(el).removeClass("lvl-target");
		                angular.element(el).removeClass("lvl-over");
		            });
		        }															        	
	        
	        },

	        link: function(scope, el, attrs, controller){	        	
	        	//console.log(scope);	        	
	        	scope.bindDragHandlers(scope, el, attrs, controller);
	        	scope.bindDropHandlers(scope, el, attrs, controller);
	        }
    	}
	}]);
	
