﻿var module = angular.module("mDesk", ['lvl.services']);

module.directive('desk', function() {
    return {
          restrict: 'E',
          templateUrl: 'js/deskTemplate.html',
          scope: {
            data:'=',
          },

          controllerAs: 'DeskController',
          controller: function(servDesk){
            var t = this;
            //данные модели 
            t.data = servDesk.getData(); 
            console.log('desk data = '); console.log(t.data);

          }
      }
  })

//набор флагов касательно работы и взаимодействия всех внутренних блоков
/*module.service('servDeskInfra',function(){
  var _inContainerFlag = false;
  var _inBlockFlag = false;

  return {
    setInContainerFlag: function(val){
      _inContainerFlag = val;
    },
    setInBlockFlag: function(val){
      _inBlockFlag = val;
    }
  }

})*/

//служба, добывающая данные  и отчасти обрабатывающая их
module.service('servDesk',function($http, uuid){
  
  /*Добывает данные про палитру блоков и страницу*/
  this.getData = function(){
  
    var data = {
      /*палитра блоков*/
      palette:[
        { type:'base', isicon:1,  status: 'permanent',
          id: uuid.new(),
          meat:{
            render : {id:0, title:'Render', content:'', active:1},
            html   : {id:1, title:'HTML', content:'это <i>html</i>', active:0},
            css    : {id:2, title:'CSS', content:'i{color:red;}', active:0},
            js     : {id:3, title:'JS', content:'console.log($("i").text());', active:0} 
          },
        },
        { type:'slot', isicon: 1, id: uuid.new(), status: 'permanent',
          meat:'' 
        },      
      ],
      /*контейнер для блоков, формирующих страницу - копируются из палитры */
      container:[  
        { type:'slot', isicon: 0, status: 'permanent',
          id: uuid.new(),
          meat:'' 
        },  
      ]
    }
    
    return data;
    }

  /*Насыщает модель временными гнезовыми блоками, 
    куда можно воткнуть новый нормальный блок*/
  this.addTempSlots = function(data){

   console.log('addTempSlots');
    var palette = data.palette;
    var newContent = [];
    var slot = ''; 
    
    //ищем в палитре блок-гнездо
    for (el in palette){
      if (palette[el].type === 'slot'){
        slot = $.extend(true,{},palette[el]);  
        slot.id = uuid.new();//новый id      
        slot.status = 'temporary';
        slot.isicon = 0; //не иконка, а развернутый блок
        break;
      }
    }
    
    
    //Проходим по контейнеру и добавляем временные гнезда между текущими блоками
    //Пишем в буфер.
    
    slot.id = uuid.new();//новый блок - обновим id
    newContent.push($.extend(true,{},slot)); //новый блок в самое начало    
    
    for (i in data.container){      
      newContent.push(data.container[i]);//вставим постоянный блок
      
      slot.id = uuid.new();//новый блок - обновим id
      newContent.push($.extend(true,{},slot)); // новый блок - вставим
    }

    data.container = newContent;
  }

  /*Удаляет из модели временные блоки*/
  this.removeTempSlots = function(data){
    console.log('removeTempSlots');

    //Пробегаем по текущему контейнеру и удаляем врем.блоки
    for (i in data.container){
      if (data.container[i].status === 'temporary'){
        data.container.splice(i,1);
      }      
    }

  }

  /*удаляет из контейнера data.container блок с данным id*/
  this.removeBlock = function(data,id){
    console.log('in removeBlock, id='+id); console.log(data);
    var inContId = objIndexOf(data.container, id);
    if (inContId !== -1){
      data.container.splice(inContId,1);
    }
    console.log(data);
  }

  });
